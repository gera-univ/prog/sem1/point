//
// Created by herman on 10/30/19.
//

#include "Point2D.h"
#include <cmath>
#include <stdexcept>

Point2D::Point2D(double x, double y) : x(x), y(y) {}

Point2D::~Point2D() {

}

double Point2D::getX() const {
    return x;
}

void Point2D::setX(double x) {
    Point2D::x = x;
}

double Point2D::getY() const {
    return y;
}

void Point2D::setY(double y) {
    Point2D::y = y;
}

void Point2D::setXY(double x, double y) {
    Point2D::x = x;
    Point2D::y = y;
}

double Point2D::getRho() const {
    return sqrt(x*x + y*y);
}

double Point2D::getPhi() const {
    if (x == 0 && y == 0) throw std::logic_error("Can't convert to polar coordinates");
    return atan(y/x);
}

std::ostream& operator<<(std::ostream &ostr, const Point2D &p) {
    ostr << "(" << p.x << "," << p.y << ")";
    return ostr;
}

std::istream& operator>>(std::istream &istr, Point2D &p) {
    istr >> p.x >> p.y;
    return istr;
}


Point2D Point2D::operator+(const Point2D &p) const {
    return Point2D(x+p.x, y+p.y);
}

void Point2D::operator+=(Point2D const &p) {
    Point2D::x += p.x;
    Point2D::y += p.y;
}

Point2D Point2D::operator-(const Point2D &p) const {
    return Point2D(x-p.x, y-p.y);
}

void Point2D::operator-=(Point2D const &p) {
    Point2D::x -= p.x;
    Point2D::y -= p.y;
}

Point2D Point2D::operator*(const Point2D &p) const {
    return Point2D(x*p.x, y*p.y);
}

void Point2D::operator*=(const Point2D &p) {
    Point2D::x *= p.x;
    Point2D::y *= p.y;
}

Point2D Point2D::operator/(const Point2D &p) const {
    if (x == 0 || y == 0 || p.x == 0 || p.y == 0) throw std::logic_error("Division by zero!");
    return Point2D(x/p.x, y/p.y);
}

void Point2D::operator/=(const Point2D &p) {
    if (x == 0 || y == 0 || p.x == 0 || p.y == 0) throw std::logic_error("Division by zero!");
    Point2D::x /= p.x;
    Point2D::y /= p.y;
}


Point2D Point2D::operator+(double d) const {
    return Point2D(x+d, y+d);
}

void Point2D::operator+=(double d) {
    Point2D::x += d;
    Point2D::y += d;
}

Point2D Point2D::operator-(double d) const {
    return Point2D(x-d, y-d);
}

void Point2D::operator-=(double d) {
    Point2D::x -= d;
    Point2D::y -= d;
}

Point2D Point2D::operator*(double d) const {
    return Point2D(x*d, y*d);
}

void Point2D::operator*=(double d) {
    Point2D::x *= d;
    Point2D::y *= d;
}

Point2D Point2D::operator/(double d) const {
    if (x == 0 || y == 0 || d == 0) throw std::logic_error("Division by zero!");
    return Point2D(x/d, y/d);
}

void Point2D::operator/=(double d) {
    if (x == 0 || y == 0 || d == 0) throw std::logic_error("Division by zero!");
    Point2D::x /= d;
    Point2D::y /= d;
}

Point2D Point2D::operator-() const {
    return Point2D(-x, -y);
}

Point2D Point2D::operator+() const {
    return Point2D(x, y);
}

Point2D &Point2D::operator--() {
    x--;
    y--;
    return *this;
}

Point2D &Point2D::operator++() {
    x++;
    y++;
    return *this;
}

Point2D Point2D::operator--(int) {
    Point2D p(*this);
    x--;
    y--;
    return p;
}

Point2D Point2D::operator++(int) {
    Point2D p(*this);
    x++;
    y++;
    return p;
}

