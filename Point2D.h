//
// Created by herman on 10/30/19.
//

#ifndef POINT_POINT2D_H
#define POINT_POINT2D_H

#include <iostream>

class Point2D {
    /*
     * A class representing a point on a plane
     */
    double x, y;
public:
    Point2D(double x = 0, double y = 0);
    virtual ~Point2D();

    // cartesian coordinate getters
    double getX() const;
    void setX(double x);
    double getY() const;
    void setY(double y);
    void setXY(double x, double y);

    // polar coordinate getters
    double getRho() const;
    double getPhi() const;

    // unary operators
    Point2D operator-() const;
    Point2D operator+() const;
    Point2D& operator--();
    Point2D& operator++();
    Point2D operator--(int);
    Point2D operator++(int);

    // arithmetic and assignment operators
    Point2D operator+(const Point2D &p) const;
    void operator+=(const Point2D &p);
    Point2D operator-(const Point2D &p) const;
    void operator-=(const Point2D &p);
    Point2D operator*(const Point2D &p) const;
    void operator*=(const Point2D &p);
    Point2D operator/(const Point2D &p) const;
    void operator/=(const Point2D &p);

    Point2D operator+(double d) const;
    void operator+=(double d);
    Point2D operator-(double d) const;
    void operator-=(double d);
    Point2D operator*(double d) const;
    void operator*=(double d);
    Point2D operator/(double d) const;
    void operator/=(double d);

    // i/o operators
    friend std::ostream& operator<<(std::ostream &ostr, const Point2D &p);
    friend std::istream& operator>>(std::istream &istr, Point2D &p);
};


#endif //POINT_POINT2D_H
