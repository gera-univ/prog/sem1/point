#include <iostream>
#include <iomanip>
#include "Point2D.h"

using namespace std;

int main() {
    Point2D p1(10,20);
    Point2D p2(30,40);
    cout << setprecision(2) << p1 << p2 << " " << p1 + p2 << p1 - p2 << p1 * p2 << p1 / p2 << endl;
    cout << -p1 << +p2 << ++p1-- << --p2++ << endl;

    return 0;
}