#include <SDL.h>
#include "Point2D.h"

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480


int main(void) {
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;

    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_HEIGHT, 0, &window, &renderer);

    Point2D p1(10,20);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    for (int i = 0; i < WINDOW_WIDTH; ++i)  {
        p1+=Point2D(1,0.1);

        SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255); // рисуем точку
        SDL_RenderDrawPoint(renderer, p1.getX(), p1.getY());
    }
    SDL_RenderPresent(renderer);

    while (true) {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}